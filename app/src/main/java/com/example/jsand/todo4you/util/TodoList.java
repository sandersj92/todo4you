package com.example.jsand.todo4you.util;

public class TodoList
{
    public Integer ID;
    public String Name;
    public String Type;

    public TodoList(Integer id, String name, String type)
    {
        ID = id;
        Name = name;
        Type = type;
    }
}
