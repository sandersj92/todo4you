package com.example.jsand.todo4you.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.EditText;

import com.example.jsand.todo4you.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewItemDialog extends Dialog
{
    private INewItemEventListener listener;

    public interface INewItemEventListener
    {
        void onClickListener(String name);
    }

    public NewItemDialog(@NonNull Context context, INewItemEventListener listener)
    {
        super(context);
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_item);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.dialog_item_enter_button)
    public void enter()
    {
        EditText editText = findViewById(R.id.dialog_item_name_edittext);
        String name = editText.getText().toString();

        listener.onClickListener(name);

        dismiss();
    }
}
