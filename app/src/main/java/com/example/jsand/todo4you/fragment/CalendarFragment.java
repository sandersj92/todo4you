package com.example.jsand.todo4you.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jsand.todo4you.Constants;
import com.example.jsand.todo4you.R;
import com.example.jsand.todo4you.util.DatabaseUtil;

public class CalendarFragment extends Fragment
{
    DatabaseUtil db;
    public CalendarFragment()
    {

    }

    public static CalendarFragment newInstance(String param1, String param2)
    {
        CalendarFragment fragment = new CalendarFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        db = new DatabaseUtil(this.getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }
}
