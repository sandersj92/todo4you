package com.example.jsand.todo4you;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.jsand.todo4you.adapter.ViewPagerAdapter;
import com.example.jsand.todo4you.dialog.NewListDialog;
import com.example.jsand.todo4you.fragment.CalendarFragment;
import com.example.jsand.todo4you.fragment.TodoFragment;
import com.example.jsand.todo4you.util.DatabaseUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends FragmentActivity
{
    private ViewPager viewPager;
    private ViewPagerAdapter adapter;
    private TodoFragment todoFragment;
    private CalendarFragment calendarFragment;
    private ArrayList<Fragment> fragmentList;

    @BindView(R.id.main_button_layout)
    LinearLayout mainLayout;

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            super.handleMessage(msg);

            DatabaseUtil db = new DatabaseUtil(getBaseContext());
            todoFragment = new TodoFragment();
            todoFragment.todoLists = db.getLists();
            updateFragments();
        }
    };

    @OnClick(R.id.main_new_list_button)
    public void newList()
    {
        NewListDialog.INewListDialogEventListener listener = new NewListDialog.INewListDialogEventListener()
        {
            public void onClickListener(String name, boolean task)
            {
                String type = task ? Constants.tableType.Task.toString() : Constants.tableType.Item.toString();
                DatabaseUtil db = new DatabaseUtil(getBaseContext());
                db.addTodo(name, type);
                Message msg = new Message();
                msg.what = 1;
                handler.handleMessage(msg);
            }
        };

        NewListDialog dialog = new NewListDialog(this, listener);
        dialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        viewPager = findViewById(R.id.main_viewpager);

        DatabaseUtil db = new DatabaseUtil(this);
        todoFragment = new TodoFragment();
        todoFragment.todoLists = db.getLists();

        calendarFragment = new CalendarFragment();

        createFragments();

        adapter = new ViewPagerAdapter(getSupportFragmentManager(), fragmentList);
        viewPager.setAdapter(adapter);
    }
    
    public void createFragments()
    {
        fragmentList = new ArrayList<>();
        fragmentList.add(todoFragment);
        fragmentList.add(calendarFragment);
    }

    public void updateFragments()
    {
        createFragments();
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), fragmentList);
        viewPager.getAdapter().notifyDataSetChanged();
    }
}
