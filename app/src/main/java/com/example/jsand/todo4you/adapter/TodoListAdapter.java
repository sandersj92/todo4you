package com.example.jsand.todo4you.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jsand.todo4you.R;
import com.example.jsand.todo4you.util.TodoItem;

import java.util.ArrayList;

public class TodoListAdapter extends BaseAdapter //todo implements View.OnTouchListener
{
    GestureDetector gestureDetector;
    private LayoutInflater inflater;
    private ArrayList<TodoItem> list;
    private Context context;

    public TodoListAdapter(Context context, ArrayList<TodoItem> list)
    {
        this.list = list;
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//todo        gestureDetector = new GestureDetector(context, new SimpleGestureListener());
    }

    @Override
    public int getCount()
    {
        return list.size();
    }

    @Override
    public Object getItem(int position)
    {
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.item_todo_listview, parent,false);
        }
        TextView textView = convertView.findViewById(R.id.todo_listView_textView);
        textView.setText(list.get(position).Item);

        if (list.get(position).Completed)
        {
            textView.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            textView.setTextColor(0x80808080);
        }
        return textView;
    }
//todo        textView.setOnTouchListener(this);

//todo    @Override
//    public boolean onTouch(View v, MotionEvent event)
//    {
//        return gestureDetector.onTouchEvent(event);
//    }
//
//    private class SimpleGestureListener extends GestureDetector.SimpleOnGestureListener
//    {
//        @Override
//        public boolean onDoubleTap(MotionEvent e)
//        {
//            Toast toast;
//            toast = Toast.makeText(context, "delete", Toast.LENGTH_SHORT);
//            toast.show();
//            return super.onDoubleTap(e);
//        }
//    }
}
