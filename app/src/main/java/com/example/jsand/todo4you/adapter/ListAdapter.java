package com.example.jsand.todo4you.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.jsand.todo4you.R;
import com.example.jsand.todo4you.activity.TodoListActivity;
import com.example.jsand.todo4you.util.TodoItem;
import com.example.jsand.todo4you.util.TodoList;

import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends BaseAdapter
{
    private LayoutInflater inflater;
    private ArrayList<TodoList> list;
    private Context context;

    public ListAdapter(Context context, ArrayList<TodoList> list)
    {
        this.list = list;
        this.context = context;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount()
    {
        return list.size();
    }

    @Override
    public Object getItem(int position)
    {
        return null;
    }

    @Override
    public long getItemId(int position)
    {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.item_todo_listview, parent,false);
        }
        TextView textView = convertView.findViewById(R.id.todo_listView_textView);
        textView.setText(list.get(position).Name);

        Log.e("TODO", list.get(position).Name);
        return textView;
    }
}
