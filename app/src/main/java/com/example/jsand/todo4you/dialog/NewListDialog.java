package com.example.jsand.todo4you.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.EditText;
import android.widget.RadioButton;

import com.example.jsand.todo4you.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewListDialog extends Dialog
{
    private INewListDialogEventListener listener;

    public interface INewListDialogEventListener
    {
        void onClickListener(String name, boolean task);
    }

    public NewListDialog(@NonNull Context context, INewListDialogEventListener listener)
    {
        super(context);
        this.listener = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_todo);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.dialog_todo_enter_button)
    public void enter()
    {
        EditText editText = findViewById(R.id.dialog_todo_name_edittext);
        String name = editText.getText().toString();

        RadioButton button = findViewById(R.id.dialog_todo_task_radiobutton);
        boolean task = button.isChecked();
        listener.onClickListener(name, task);
        dismiss();
    }
}
