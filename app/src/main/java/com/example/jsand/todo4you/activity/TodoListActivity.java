package com.example.jsand.todo4you.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.telecom.Call;
import android.text.method.ScrollingMovementMethod;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jsand.todo4you.R;
import com.example.jsand.todo4you.adapter.TodoListAdapter;
import com.example.jsand.todo4you.dialog.NewItemDialog;
import com.example.jsand.todo4you.util.DatabaseUtil;
import com.example.jsand.todo4you.util.TodoItem;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class TodoListActivity extends AppCompatActivity implements View.OnTouchListener
{
    private GestureDetector gestureDetector;
    private int ID;
    private String Name;
    private String Type;
    private ListView listView;
    private TodoListAdapter adapter;
    private ArrayList<TodoItem> itemList;

    @SuppressLint("HandlerLeak")
    Handler handler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            super.handleMessage(msg);

            DatabaseUtil db = new DatabaseUtil(getBaseContext());
            itemList = db.getItems(ID);

            Intent intent = getIntent();
            finish();
            startActivity(intent);
            overridePendingTransition(R.anim.nothing, R.anim.nothing);
        }
    };

    @OnClick(R.id.activity_todo_add_item)
    public void addItem()
    {
        NewItemDialog.INewItemEventListener listener = new NewItemDialog.INewItemEventListener()
        {
            @Override
            public void onClickListener(String name)
            {
                DatabaseUtil db = new DatabaseUtil(getBaseContext());
                db.addItem(ID, name);
                Message msg = new Message();
                msg.what = ID;
                handler.handleMessage(msg);
            }
        };

        NewItemDialog dialog = new NewItemDialog(this, listener);
        dialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_list);
        ButterKnife.bind(this);
        gestureDetector = new GestureDetector(this, new SimpleGestureListener());

        Intent intent = getIntent();
        Bundle todoList = intent.getBundleExtra("TodoList");
        ID = todoList.getInt("ID");
        Name = todoList.getString("Name");
        Type = todoList.getString("Type");

        TextView textView = findViewById(R.id.activity_todo_title);
        textView.setText(Name);
        textView.setMovementMethod(new ScrollingMovementMethod());

        DatabaseUtil db = new DatabaseUtil(this);
        itemList = db.getItems(ID);

        listView = findViewById(R.id.activity_todo_listview);
        adapter = new TodoListAdapter(this, itemList);

        listView.setAdapter(adapter);

        listView.setOnTouchListener(this);
        listView.setClickable(true);
        listView.setLongClickable(true);
        listView.setFocusable(true);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                DatabaseUtil db = new DatabaseUtil(getBaseContext());
                Message msg = new Message();

                if (!itemList.get(position).Completed)
                    db.setCompleted(itemList.get(position).ID);
                else
                    db.setNotCompleted(itemList.get(position).ID);

                handler.handleMessage(msg);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                DatabaseUtil db = new DatabaseUtil(getBaseContext());
                Message msg = new Message();

                if (!itemList.get(position).Completed)
                    db.setCompleted(itemList.get(position).ID);
                else
                    db.removeItem(itemList.get(position).ID);

                handler.handleMessage(msg);
                return true;
            }
        });
    }

    @Override
    public boolean onTouch(View v, MotionEvent event)
    {
        return gestureDetector.onTouchEvent(event);
    }

    private class SimpleGestureListener extends GestureDetector.SimpleOnGestureListener
    {
        @Override
        public boolean onDoubleTap(MotionEvent e)
        {
            Toast toast;
            toast = Toast.makeText(getBaseContext(), "double", Toast.LENGTH_SHORT);
            toast.show();
            return super.onDoubleTap(e);
        }
    }
}
