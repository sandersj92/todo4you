package com.example.jsand.todo4you.util;

public class TodoItem
{
    public Integer ID;
    public Integer ListId;
    public String Item;
    public boolean Completed;

    public TodoItem(Integer id, Integer listId, String item, String completed)
    {
        ID = id;
        ListId = listId;
        Item = item;
        Completed = Boolean.parseBoolean(completed);
    }
}
