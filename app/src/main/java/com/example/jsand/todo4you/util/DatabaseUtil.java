package com.example.jsand.todo4you.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DatabaseUtil extends SQLiteOpenHelper
{
    SQLiteDatabase db;
    public static final String DATABASE_NAME = "todo4you.db";
    public static final String TABLE_NAME = "REFERENCE";
    public static final String TASK_TABLE_NAME = "TASKS";
    public static final String LIST_ID = "ID";
    public static final String LIST_NAME = "NAME";
    public static final String LIST_TYPE = "TYPE";
    public static final String ITEM_ID = "ID";
    public static final String ITEM_LIST_ID = "LIST_ID";
    public static final String ITEM = "ITEM";
    public static final String COMPLETED = "COMPLETED";
    public Cursor cursor;

    public DatabaseUtil(Context context)
    {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("create table if not exists REFERENCE (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, TYPE TEXT)");
        db.execSQL("create table if not exists TASKS (ID INTEGER PRIMARY KEY AUTOINCREMENT, LIST_ID INTEGER, ITEM TEXT, COMPLETED TEXT)");
        Log.d("database", "On Create");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
    }

    public void addTodo(String name, String type)
    {
        db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put(LIST_NAME, name);
        value.put(LIST_TYPE, type);
        db.insert(TABLE_NAME, null, value);
        db.close();
    }

    public void removeTodo(int id)
    {
        db = this.getWritableDatabase();
        db.delete(TABLE_NAME, "ID = "+id, null);
        db.delete(TASK_TABLE_NAME, "LIST_ID = "+id, null);
        db.close();
    }

    public ArrayList<TodoList> getLists()
    {
        ArrayList<TodoList> TodoLists = new ArrayList<>();
        String[] projections = {LIST_ID, LIST_NAME, LIST_TYPE};
        db = this.getReadableDatabase();
        cursor = db.query(TABLE_NAME, projections, null, null, null, null, null);

        while (cursor.moveToNext())
        {
            int id = cursor.getInt(cursor.getColumnIndex(LIST_ID));
            String name = cursor.getString(cursor.getColumnIndex(LIST_NAME));
            String type = cursor.getString(cursor.getColumnIndex(LIST_TYPE));

            TodoLists.add(new TodoList(id, name, type));
        }
        db.close();
        return TodoLists;
    }

    public void addItem(int listId, String item)
    {
        db = this.getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put(ITEM_LIST_ID, listId);
        value.put(ITEM, item);
        value.put(COMPLETED, "false");
        db.insert(TASK_TABLE_NAME, null, value);
        db.close();
    }

    public void removeItem(int itemId)
    {
        db = this.getWritableDatabase();
        db.delete(TASK_TABLE_NAME, "ID = "+itemId, null);
        db.close();
    }

    public ArrayList<TodoItem> getItems(int listID)
    {
        ArrayList<TodoItem> todoItems = new ArrayList<>();
        db = this.getReadableDatabase();
        cursor = db.rawQuery("select * from TASKS where TASKS.LIST_ID = "+listID, null);

        while (cursor.moveToNext())
        {
            int id = cursor.getInt(cursor.getColumnIndex(ITEM_ID));
            int listId = cursor.getInt(cursor.getColumnIndex(ITEM_LIST_ID));
            String item = cursor.getString(cursor.getColumnIndex(ITEM));
            String completed = cursor.getString(cursor.getColumnIndex(COMPLETED));

            todoItems.add(new TodoItem(id, listId, item, completed));
        }
        db.close();
        return todoItems;
    }

    public void setCompleted(int id)
    {
        db = this.getWritableDatabase();
        db.execSQL("update TASKS set COMPLETED = 'true' where ID = "+id);
    }

    public void setNotCompleted(int id)
    {
        db = this.getWritableDatabase();
        db.execSQL("update TASKS set COMPLETED = 'false' where ID = "+id);
    }
}
