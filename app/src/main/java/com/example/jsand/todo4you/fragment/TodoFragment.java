package com.example.jsand.todo4you.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jsand.todo4you.Constants;
import com.example.jsand.todo4you.MainActivity;
import com.example.jsand.todo4you.R;
import com.example.jsand.todo4you.activity.TodoListActivity;
import com.example.jsand.todo4you.adapter.ListAdapter;
import com.example.jsand.todo4you.util.DatabaseUtil;
import com.example.jsand.todo4you.util.TodoList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class TodoFragment extends Fragment
{
    private DatabaseUtil db;
    private Button newItem;
    private static String listName = "ToDo Lists";
    public ArrayList<TodoList> todoLists = new ArrayList<>();
    private ListView listView;
    private ListAdapter adapter;

    public TodoFragment()
    {
    }

    public TodoFragment newInstance(ArrayList<TodoList> lists)
    {
        TodoFragment fragment = new TodoFragment();
        todoLists = lists;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        db = new DatabaseUtil(this.getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_todo, container, false);
        TextView textView = view.findViewById(R.id.fragment_todo_title);
        textView.setText(listName);

        listView = view.findViewById(R.id.todo_listview);
        adapter = new ListAdapter(getContext(), todoLists);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            Intent intent = new Intent();
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                intent.setClass(getActivity(), TodoListActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt("ID", todoLists.get(position).ID);
                bundle.putString("Name", todoLists.get(position).Name);
                bundle.putString("Type", todoLists.get(position).Type);
                intent.putExtra("TodoList", bundle);
                startActivity(intent);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
            {
                DatabaseUtil db = new DatabaseUtil(getContext());
                Message msg = new Message();

                db.removeTodo(todoLists.get(position).ID);

                return true;
            }
        });
        return view;
    }
}
