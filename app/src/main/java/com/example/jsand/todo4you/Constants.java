package com.example.jsand.todo4you;

public class Constants
{
    public final String databaseName = "todo4you.db";

    public enum tableType
    {
        Task,
        Item,
        Calendar
    }
}
